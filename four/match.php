<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <title>จับคู่</title>
    </head>
    <body>
        <div class="container" style="">
            <h1>จับคู่</h1>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="sText">ข้อความ</label>
                        <input type="text" class="form-control" id="sText" name="sText">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="iResult">ผลลัพธ์</label>
                        <div id="iMatch"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button id="btnSubmit" class="btn btn-primary">Click</button>
                        <button id="btnResrt" class="btn btn-danger">ล้างค่า</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="../jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
    <script>
        $( document ).ready(function() {
            $("#btnSubmit").on('click', function() {
                var sText = $('#sText').val();
                if (sText == "") {
                    alert('กรุณาใส่ข้อความ');
                } else {
                    $("#iMatch").css("display","none")
                    $('#iMatch').empty();
                    var result = match(sText);
                    $("#iMatch").append(result)
                }
            });
            function match(sText){
                $("#iMatch").css("display","block")
                split = sText.split("");
                var left = [];
                var right = [];
                for (var i = 0; i < split.length; i++) {
                    switch (split[i]) {
                        case '(':
                            left.push(1);
                            break;
                        case '{':
                            left.push(2);
                            break;
                        case '[':
                            left.push(3);
                            break;
                        case ')':
                            right.push(1);
                            break;
                        case '}':
                            right.push(2);
                            break;
                        case ']':
                            right.push(3);
                            break;
                    }
                }

                right.reverse();

                var check = '';
                if (left.length != right.length) {
                    html = '<span>false</span>';
                    return html;
                } else if (left.length == 0 || right.length == 0) {
                    html = '<span>false</span>';
                    return html;
                } else {
                    for (var i = 0; i < left.length; i++) {

                        if (left[i] != right[i]) {
                            check = 2;
                            break;
                        } else {
                            check = 1;
                        }
                    }

                    if (check == 1) {
                        html = '<span>true</span>';
                        return html;
                    } else {
                        html = '<span>false</span>';
                        return html;
                    }
                }
            }

            $("#btnResrt").on('click', function() {
                $('#sText').val('');
                $('#iMatch').empty();
                $("#iMatch").css("display","none");
            });
        });
    </script>
</html>
