<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <title>calculate</title>
    </head>
    <body>
        <div class="container" style="">
            <h1>คำนวณหาผลลัพธ์ m ถึง n</h1>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="iM">m</label>
                        <input type="number" class="form-control" id="iM" name="iM">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="iN">n</label>
                        <input type="number" class="form-control" id="iN" name="iN">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="iN">ผลลัพธ์</label>
                        <input type="number" class="form-control" id="iResult" name="iResult" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button id="btnSubmit" class="btn btn-primary">คำนวณ</button>
                        <button id="btnResrt" class="btn btn-danger">ล้างค่า</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="../jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
    <script>
        $( document ).ready(function() {
            $("#btnSubmit").on('click', function() {
                var iM = parseInt($('#iM').val());
                var iN = parseInt($('#iN').val());
                if (iM == "") {
                    alert('กรุณากรอกค่า M');
                } else if (iN == "") {
                    alert('กรุณากรอกค่า N');
                } else if (iM > iN) {
                    alert('กรุณากรอกค่า M ให้น้อยกว่าค่า N');
                } else {
                    var result = calculate(iM,iN);
                    $('#iResult').val(result);
                }
            });
            function calculate(iM,iN){
                var sum = 0;
                for (var i = iM; i <= iN; i++) {
                    sum += i;
                }
                return sum;
            }

            $("#btnResrt").on('click', function() {
                $('#iM').val('');
                $('#iN').val('');
                $('#iResult').val('');
            });
        });
    </script>
</html>
