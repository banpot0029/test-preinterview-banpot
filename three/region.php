<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <title></title>
    </head>
    <body>
        <div class="container" style="">
            <div class="row">

            </div>
        </div>
        <script src="../jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
    <script>
        $( document ).ready(function() {
            var aCountry = getCountry();
            var aRegion = getRegion();
            var result = {};
            for (var i = 0; i < aCountry.length; i++) {
              var resultRegion = checkRegion(aCountry[i].regionId);
              result[aCountry[i].abbr] = resultRegion
            }
            console.log(result);
            function getCountry(){
              var aCountry = [
                {name: "Thailand", abbr: "th", remark: "", regionId: 1},
              	{name: "Singapore", abbr: "sg", remark: "This is sg", regionId: 1},
              	{name: "United State", abbr: "US", remark: "", regionId: 7},
              ]
              return aCountry;
            }
            function getRegion(){
              var aRegion = [
                {id: 1, name: "Asia"},
              	{id: 2, name: "South America"},
              	{id: 7, name: "North America"}
              ]
              return aRegion;
            }
            function checkRegion(regionId){
              var checkRegion = '';
              for (var i = 0; i < aRegion.length; i++) {
                if (regionId == aRegion[i].id) {
                  checkRegion = aRegion[i].name
                }
              }
              return checkRegion;
            }
        });
    </script>
</html>
